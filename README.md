### How do I get set up? ###


## First Methodology ##
* Clone the repo
* npm install
* Follow step 2 & 3 [here](http://webdriver.io/guide.html)
* Download [ChromeDriver](https://sites.google.com/a/chromium.org/chromedriver/downloads)
* Extract the downloaded zip in the project directory
* Start selenium standalone server

```
#!bash

java -jar -Dwebdriver.gecko.driver=./geckodriver selenium-server-standalone-3.0.1.jar
```
* node test.js
* in new terminal
```
#!bash

curl -X POST http://localhost:9000/ -H 'content-type: application/json' -d '{"email":"email", "password":"password"}'
```


## Second Methodology ##

* clone the repo
* npm install
* npm install selenium-standalone@latest -g
* selenium-standalone install
* selenium-standalone start
* node test.js
* in new terminal

```
#!bash
curl -X POST http://localhost:9000/ -H 'content-type: application/json' -d '{"email":"email", "password":"password"}'

```